# REST API test application.

## How to install

1) You must have composer.
If you do not have yet, please refer to [https://getcomposer.org/](https://getcomposer.org/) 

2) You should have one of these DB servers: MySQL, SQLite or PostgreSQL.

3) Create database with any name.

4) Edit database preferences in `config/db.php`.

5) Load dump from `dump.sql` (this dump for MySQL).

6) Set up virtual host on Apache or Nginx, make sure what DocumentRoot relate on `web` directory.


## How to use

You can use any tool for making HTTP queries: postman, curl (for console geeks :)), etc.

1) For testing first end-point (**Organizations and Relations creation**) just send POST request to `/organizations` url with this body:
```
{
    "org_name": "Paradise Island",
    "daughters": [{
        "org_name": "Banana tree",
        "daughters": [{
            "org_name": "Yellow Banana"
        }, {
            "org_name": "Brown Banana"
        }, {
            "org_name": "Black Banana"
        }]
    }, {
        "org_name": "Big banana tree",
        "daughters": [{
            "org_name": "Yellow Banana"
        }, {
            "org_name": "Brown Banana"
        }, {
            "org_name": "Green Banana"
        }, {
            "org_name": "Black Banana",
            "daughters": [{
                "org_name": "Phoneutria Spider"
            }]
        }]
    }]
}
```

Make sure what you use `JSON (application/json)` type of body.

2) For testing second end-point (**getting relation of Organization**) just send GET request to `/organizations/{organizationName}` url (organizationName should be urlencoded).
You can use pagination parameters: just use `limit` and `offset` parameters. For example: `/organizations/Brown%20Banana?limit=3&offset=1`. Default values for `limit` is **100**, for `offset` - **0**.