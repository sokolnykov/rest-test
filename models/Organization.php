<?php

namespace models;

use ActiveRecord\Model as ActiveRecordModel;

/**
 * This is the model class for table "organizations".
 *
 * @property string $name
 */
class Organization extends ActiveRecordModel
{
    const DEFAULT_BUNDLE_LIMIT = 100;

    public static function createBundle(array $data): array
    {
        $resultArray = [
            'createdOrganizationCount' => 0,
            'createdRelationCount' => 0
        ];

        $resultArray = self::parseCreationData($data['org_name'], $data['daughters'], $resultArray);

        return $resultArray;
    }


    public static function getBundle(string $name, ?int $limit, ?int $offset): array
    {
        $limit = $limit ? $limit : self::DEFAULT_BUNDLE_LIMIT;
        $offset = $offset ? $offset : 0;

        $modelName = urldecode($name);
        $parents = Relation::find('all', ['conditions' => ['child=?', $modelName]]);
        $parentsArray = self::modelsToArray($parents, 'parent', 'parent');
        $children = Relation::find('all', ['conditions' => ['parent=?', $modelName]]);
        $childrenArray = self::modelsToArray($children, 'child', 'daughter');

        $resultArray = array_merge($parentsArray, $childrenArray);

        $sistersResultArray = [];
        foreach ($parents as $parent) {
            $sisters = Relation::find('all', ['conditions' => ['parent=? and child<>?', $parent->parent, $modelName]]);
            $sistersArray = self::modelsToArray($sisters, 'child', 'sister');

            foreach ($sistersArray as $array) {
                if (!in_array($array, $sistersResultArray)) $sistersResultArray[] = $array;
            }
        }
        $resultArray = array_merge($resultArray, $sistersResultArray);


        // result array sorting
        $dataNames = [];
        foreach($resultArray as $key => $arr){
            $dataNames[$key] = $arr['org_name'];
        }
        array_multisort($dataNames, SORT_ASC, $resultArray);


        // result limitation
        $resultArray = array_splice($resultArray, $offset, $limit);

        return $resultArray;
    }


    private static function parseCreationData(string $parentName, array $childrenArray, array $resultArray): array
    {
        if (!self::findModel($parentName)) {
            $organization = new Organization();
            $organization->name = $parentName;
            $organization->save();

            $resultArray['createdOrganizationCount']++;
        }

        foreach ($childrenArray as $child) {
            if (!self::findModel($parentName)) {
                $organization = new Organization();
                $organization->name = $parentName;
                $organization->save();

                $resultArray['createdOrganizationCount']++;
            }

            if (!Relation::find('all', ['conditions' => ['parent=? and child=?', $parentName, $child['org_name']]])) {
                $relation = new Relation();
                $relation->parent = $parentName;
                $relation->child = $child['org_name'];
                $relation->save();

                $resultArray['createdRelationCount']++;
            }

            $resultArray = self::parseCreationData($child['org_name'], $child['daughters'] ? $child['daughters'] : [], $resultArray);
        }

        return $resultArray;
    }


    private static function findModel(string $name)
    {
        return Organization::find('one', ['conditions' => ['name=?', $name]]);
    }


    private static function modelsToArray(array $models, string $nameField, string $type): array
    {
        $resultArray = [];
        foreach ($models as $model) {
            $resultArray[] = [
                'relationship_type' => $type,
                'org_name' => $model->{$nameField}
            ];
        }

        return $resultArray;
    }
}