<?php

namespace models;

use ActiveRecord\Model as ActiveRecordModel;

/**
 * This is the model class for table "relations".
 *
 * @property string $parent
 * @property string $child
 */
class Relation extends ActiveRecordModel
{

}