<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/db.php';

use Respect\Rest\Router;
use Respect\Validation\Validator as V;
use models\Organization;


$router = new Router;

// route for creation new organization
$router->post('/organizations', function () {
    return Organization::createBundle(json_decode(file_get_contents('php://input'), true));
})->when(function () {
    return V::json()->validate(file_get_contents('php://input'));
})->accept([
    'application/json' => 'json_encode'
]);


// route for getting information about organization
$router->get('/organizations/*', function ($name) {
    return Organization::getBundle($name, $_GET['limit'], $_GET['offset']);
})->when(function ($name) {
    return (V::stringType()->validate($name) && V::notEmpty()->validate($name));
})->accept([
    'application/json' => 'json_encode'
]);