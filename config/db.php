<?php

use ActiveRecord\Config as ARConfig;

ARConfig::initialize(function($cfg)
{
    $cfg->set_model_directory(__DIR__ . '/../models');
    $cfg->set_connections([
        'development' => 'mysql://root:password@localhost/db-name'
    ]);
});