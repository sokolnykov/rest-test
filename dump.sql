CREATE TABLE `organizations` (
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `relations` (
  `parent` varchar(255) NOT NULL,
  `child` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `organizations`
  ADD PRIMARY KEY `i_organizations_name` (`name`);

ALTER TABLE `relations`
  ADD UNIQUE KEY `i_relations_parent_child` (`parent`,`child`),
  ADD KEY `i_relations_parent` (`parent`),
  ADD KEY `i_relations_child` (`child`);